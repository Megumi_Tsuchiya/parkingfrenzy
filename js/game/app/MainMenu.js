var car;
var handle;
var upBtn;
var downBtn;
var cursors;
var goal;
var wall;
var walls;
var debugText;
App.MainMenu = function(game) {};
App.MainMenu.prototype = {

    create: function() {
        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.stage.backgroundColor = 0xdcdcdc;
        //this.stage.backgroundColor = 0x99cc99;

        //handle = this.add.sprite(750, 520,'handleImg');
        goal = this.add.sprite(600, 300, 'parkingAreaImg');
        car = this.add.sprite(this.world.width, 600, 'carImg');
        //upBtn = this.add.button((320-146)/2, 200, 'arrowUpImg', actionOnClickUpBtn, this, 1, 0, 2);
        //downBtn = this.add.button((320-146)/2, 400, 'arrowDownImg', actionOnClickDownBtn, this, 1, 0, 2);
        //upBtn = this.add.sprite(10, 555, 'arrowUpImg');
        //downBtn = this.add.sprite(10, 600, 'arrowDownImg');

        this.physics.enable(car, Phaser.Physics.ARCADE);
        this.physics.enable(goal, Phaser.Physics.ARCADE);
        //this.physics.enable(handle, Phaser.Physics.ARCADE);

        car.anchor.setTo(0.5, 0.5);
        car.body.collideWorldBounds = true;
        car.body.maxVelocity.setTo(500, 500);

        goal.anchor.setTo(0.5, 0.5);
        goal.body.setSize(10, 5);

        // wallsグループの生成
        walls = this.add.group();
        // wallsグループのオブジェクトは、すべて物理演算をオンにする
        walls.enableBody = true;
        // ここで、wallsグループに壁(wall)を追加する
        //wall = walls.create(0, this.world.height - 64, 'wallImg');
        wall = walls.create(300, 400, 'wallImg');
        //  壁のサイズをゲームの幅にフィットさせる (スプライトのオリジナルサイズは、400x32)
        wall.scale.setTo(2, 2);

        //  壁を固定する
        wall.body.immovable = true;


        //handle.inputEnabled = true;
        //handle.events.onInputDown.add(onDown, this);

        cursors = this.input.keyboard.createCursorKeys();

        debugText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

        //upBtn.inputEnabled = true;
        //upBtn.input.enableDrag(true);
        //handle.events.onInputDown.add(onDown, this);

     },

    update: function () {

        car.body.velocity.x = 0;
        car.body.velocity.y = 0;
        car.body.angularVelocity = 0;

        if (cursors.left.isDown) {
            //handle.body.rotateLeft(100);
            //car.body.angularVelocity = -200; //大きくなるほど回転速度が上がる
         }else if (cursors.right.isDown) {
            //handle.body.rotateRight(100);
            //car.body.angularVelocity = 200;
        }else {
            //handle.body.setZeroRotation();
        }

        if (cursors.up.isDown || cursors.down.isDown){
            if (cursors.left.isDown) {
                car.body.angularVelocity = -200; //大きくなるほど回転速度が上がる
            }else if (cursors.right.isDown) {
                car.body.angularVelocity = 200;
            }
            if (cursors.up.isDown){
                this.physics.arcade.velocityFromAngle(car.angle, -300, car.body.velocity);
            }// 第二引数は速度
            else if (cursors.down.isDown){
                this.physics.arcade.velocityFromAngle(car.angle, 300, car.body.velocity);
            }
        }
         //else{car.body.setZeroVelocity();}

        /*if (cursors.up.isDown) {
            // If the LEFT key is down, set the player velocity to move left
            car.body.acceleration.x = -1500;
        } else if (cursors.down.isDown) {
            // If the RIGHT key is down, set the player velocity to move right
            car.body.acceleration.x = 1500;
        } else {
            // Stop the player from moving horizontally
            car.body.acceleration.x = 0;
            car.body.velocity.x = 0;
        }*/

        //  車と壁グループに衝突判定を追加
        this.physics.arcade.collide(car, walls);

        var dis = Phaser.Point.distance(car, goal);
        debugText.text = 'distance: ' + dis
        //ゴール判定テスト用
        /*if (this.physics.arcade.overlap(car, goal)){
            this.stage.backgroundColor = '#696969';
        }else{
            this.stage.backgroundColor = 0xdcdcdc;
        }*/
        //ゴール判定
        this.physics.arcade.overlap(car, goal, isGoal, null, this);

    },

    render: function () {
        //this.debug.body(goal);
        //this.debug.inputInfo(car,32, 32,'rgb(0,0,0)');
        //this.debug.text( "This is debug text", 300, 300 );
    }
};

function onDown () {

    this.stage.backgroundColor = '#696969';

}

function isGoal () {

        if (Phaser.Point.distance(car, goal) < 20) {

            this.stage.backgroundColor = '#696969';
            var text = "Well Done!!";
            var style = {font: "45px Arial", fill: "#ffffff", align: "center"};

            var t = this.add.text(this.world.centerX, this.world.centerY, text, style);
        }

}
function collisionHandler (obj1, obj2) {

    this.stage.backgroundColor = '#992d2d';

}

function actionOnClickUpBtn () {

    //car.body.velocity.x=300;
    this.physics.arcade.velocityFromAngle(car.angle, -300, car.body.velocity);

}
function actionOnClickDownBtn () {

    car.body.velocity.x=-300;

}